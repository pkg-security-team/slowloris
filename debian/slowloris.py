#!/usr/bin/python3

import os
import sys
sys.path.insert(0, '/usr/share/slowloris')

from slowloris import main

if __name__ == '__main__':
    main()

Source: slowloris
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.2
Homepage: https://github.com/gkbrk/slowloris
Vcs-Browser: https://salsa.debian.org/pkg-security-team/slowloris
Vcs-Git: https://salsa.debian.org/pkg-security-team/slowloris.git

Package: slowloris
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Security testing tool for web servers
 Open source tool used to perform Denial of Service (DoS) attacks on web
 servers. Slowloris exploits a vulnerability in web servers called “Keep-Alive”
 to exhaust server resources and make them inaccessible.
 .
 Slowloris offers a number of configurable options, allowing fine-grained
 control over request behavior. Some of which include:
  - Web Server Port: It is possible to specify the web server port to be
    the target of the action, usually port 80.
  - Number of Sockets: You can determine the number of simultaneous sockets
    that the tool will use to send requests to the server.
  - Log Increase (Verbose): The level of detail of the information logged
    during the run can be adjusted, allowing a more detailed analysis of the
    results.
  - User agent randomization: Slowloris can randomize user agents on each
    request, making it difficult to detect the destination server.
  - Use of SOCKS5 proxy: The tool supports the use of a SOCKS5 proxy to further
    hide the source of requests.
  - Use of HTTPS for Requests: In addition, it is possible to configure the
    tool to use the HTTPS protocol for requests, adding another layer of
    obscurity.
  - Time interval between headers: You can specify a time interval between
    sending request headers, controlling the rate of requests.
